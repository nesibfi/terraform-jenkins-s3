# terraform-jenkins-s3



## Project Scenario:

Create the Jenkins server using Terraform so that it can be used in other environments and so that changes to the environment are better tracked.


## Project Objective:

- [ ] Deploy 1 EC2 Instance in your Default VPC.
- [ ] Bootstrap the EC2 instance with a script that will install and start Jenkins.
- [ ] Create and assign a Security Group to the Jenkins EC2 that allows traffic on port 22 and allows traffic from port 8080.
- [ ] Create a S3 bucket for your Jenkins Artifacts that is not open to the public.
- [ ] Verify that you can reach your Jenkins install via port 8080 in your browser.

```
cd existing_repo
terraform init
terraform fmt
terraform plan
terraform apply -auto-approve
```

## Verify Jenkins is running
- [ ] SSH to Jenkins instance
```
sudo systemctl status jenkins
```

## Access Jenkins via a web browser
```
http://<instance_ip>:8080
```

## Verify S3 Permissions
- [ ] Configure the AWS CLI on the instance
```
aws configure
aws s3 ls
echo "verify we can use the PutItem API call" > testUpload.txt
aws s3 cp test.txt s3://<bucket_name>
```
